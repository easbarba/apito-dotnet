# ApitoBackend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ApitoBackend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ApitoBackend. If not, see <https://www.gnu.org/licenses/>.

ARG DOTNET_IMAGE=mcr.microsoft.com/dotnet/sdk:7.0

FROM $DOTNET_IMAGE as builder
WORKDIR /app
COPY ./ApitoBackend.sln
RUN dotnet restore

FROM $DOTNET_IMAGE
MAINTAINER EAS Barbosa<easbarba@outlook.com>
WORKDIR /app
COPY --from=builder /app/target/apitobackend-*.jar ./apitobackend.jar
ENTRYPOINT [ "dotnet","test" ]
