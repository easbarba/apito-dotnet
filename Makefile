# ApitoBackend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ApitoBackend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ApitoBackend. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include envs/.env.*
-include .envrc

RUNNER ?= podman
POD_NAME := apito
NAME := apito-aspnet
VERSION := $(shell cat .version)

# ======================================= POD

logs:
	${RUNNER} logs ${NAME}

stats:
	${RUNNER} pod stats ${POD_NAME}

base: initial database server

stop:
	${RUNNER} pod rm --force ${POD_NAME}
	${RUNNER} container rm --force ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}
	${RUNNER} container rm --force ${NAME}

initial:
	${RUNNER} pod create \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--publish ${QUARKUS_DEBUG_PORT}:${QUARKUS_DEBUG_PORT} \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--name ${POD_NAME}

database:
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME}\
		--name ${DATABASE_NAME} \
		--env-file ./envs/.env.db \
		--env POSTGRES_PASSWORD=${POSTGRES_PASSWORD} -e POSTGRES_USER=${POSTGRES_USER} -e POSTGRES_DB=${POSTGRES_DB} \
		--volume ${DATABASE_DATA}:/var/lib/postgresql/data:Z \
		${DATABASE_IMAGE}

repl-db:
	podman exec -it ${DATABASE_NAME} psql -U ${POSTGRES_USER} -d ${POSTGRES_DB}

server:
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME}\
		--name apito-server \
		--volume ${PWD}:/var/www/app \
		--volume ./ops/nginx-default.conf:/etc/nginx/conf.d/default.conf \
		${SERVER_IMAGE}

prod:
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--name ${NAME} \
		${NAME}:${VERSION}

start:
	${RUNNER} container rm --force ${NAME}-start
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-start \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		${DOTNET_IMAGE} \
		bash -c 'dotnet run --lp http'

repl:
	${RUNNER} container rm --force ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --interactive --tty \
		--name ${NAME}-repl \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.security \
		--env-file ./envs/.env.db \
		--quiet \
		${DOTNET_IMAGE} \
		bash

command:
	${RUNNER} container rm --force ${NAME}-command
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-command \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		--quiet \
		${DOTNET_REPL_IMAGE} \
		bash -c 'dotnet $(shell cat commands | fzf)'

test:
	${RUNNER} container rm --force ${NAME}-test
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		${DOTNET_IMAGE} \
		bash -c 'dotnet test'

# ============================================= TASKS
build:
	${RUNNER} build --file ./Dockerfile --tag ${USER}/${NAME}:${VERSION}

publish:
	# ---------------------- API IMAGE
	${RUNNER} tag ${NAME}:${VERSION}  ${USERHUB}/${NAME}:${VERSION}
	${RUNNER} push ${USERHUB}/${NAME}:${VERSION}
	# ---------------------- DATABASE IMAGE
	${RUNNER} commit ${DATABASE_NAME}  ${USERHUB}/${DATABASE_NAME}:${VERSION}
	${RUNNER} push ${USERHUB}/${DATABASE_NAME}:${VERSION}

fmt:
	clang-format -i ./src/main/java/dev/easbarba/apitobackend/*/***
	clang-format -i ./src/test/java/dev/easbarba/apitobackend/*/***

system:
	guix shell --pure --container

.PHONY := repl system publish build fmt initial database server stats logs command add repl-db
.DEFAULT_GOAL := test
